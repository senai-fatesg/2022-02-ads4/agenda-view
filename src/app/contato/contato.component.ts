import { Component, OnInit } from '@angular/core';
import { ContatoService } from '../contato.service';

@Component({
  selector: 'app-contato',
  templateUrl: './contato.component.html',
  styleUrls: ['./contato.component.css']
})
export class ContatoComponent implements OnInit {

  public contato:any = {"nome": '', "telefone": ''};
  public contatos:any = [];


  constructor(private contatoService: ContatoService) { }

  ngOnInit(): void {
    this.listar();
  }

  listar(){
    this.contatoService.get().subscribe(r => {
      this.contatos = r;
    });
  }

  addContato(){
    this.contatoService.post(this.contato).subscribe(r => {
      this.listar();
      this.contato =  {"nome": '', "telefone": ''}
    });
  }

  delete(id: any){
    this.contatoService.delete(id).subscribe(r => {
      this.listar();
    });
  }

  selecionarContato(c: any){
    this.contato = c;
  }

}
