# Angular

## Instalação do angular

Instale o nodejs: https://nodejs.org/en/download/

Depois execute o código no prompt (https://angular.io/guide/setup-local):

```bash
npm install -g @angular/cli
```

## Criação do projeto e passos iniciais

ng new agenda-view
cd agenda-view

ng g c contato

const routes: Routes = [{path: 'contato', component: ContatoComponent},];

Teste: http://localhost:4200/contato funciona?

Agora modifique o código de app.component.html para:

```xml
<router-outlet></router-outlet>
```

Teste http://localhost:4200/contato funciona?

## BootStrap

Configurações iniciais

https://getbootstrap.com/docs/4.0/getting-started/introduction/

```
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

```

## Diretivas

https://angular.io/guide/built-in-directives#displaying-and-updating-properties-with-ngmodel

https://cursos.alura.com.br/forum/topico-porque-e-em-ngmodel-168794

### ngModel

```ts
import { FormsModule } from '@angular/forms'; // <--- JavaScript import from Angular
/* . . . */
@NgModule({
  /* . . . */

  imports: [
    BrowserModule,
    FormsModule // <--- import into the NgModule
  ],
  /* . . . */
})
export class AppModule { }
```

```
<label for="example-ngModel">[(ngModel)]:</label>
<input [(ngModel)]="currentItem.name" id="example-ngModel">
```

## Interpolation

https://angular.io/guide/interpolation

